const express = require('express');
const bodyParser = require('body-parser');
const bcrypt = require('bcrypt-nodejs');
const port = process.env.PORT || 3000;
const cors = require('cors');
const knex = require('knex');

const db = knex({
  client: 'pg',
  connection: {
    host: '127.0.0.1',
    user: 'matteosoresini',
    password: '',
    database: 'process.env.full-stack-charts',
  },
});

const app = express();

app.use(cors());
app.use(bodyParser.json());

// app.get('/', (req, res) => {
//   res.send(database.users);
// });

app.post('/signin', (req, res) => {
  db.select('email', 'hash')
    .from('login')
    .where('email', '=', req.body.email)
    .then((data) => {
      const isValid = bcrypt.compareSync(req.body.password, data[0].hash);
      if (isValid) {
        return db
          .select('email', 'id')
          .from('users')
          .where('email', '=', req.body.email)
          .then((user) => {
            res.json(user[0]);
          })
          .catch((err) => res.status(400).json('unable to get user'));
      } else {
        res.status(400).json('wrong credentials');
      }
    })
    .catch((err) => res.status(400).json('wrong credentials'));
});

app.post('/register', (req, res) => {
  const { email, name, password } = req.body;
  const hash = bcrypt.hashSync(password);
  db.transaction((trx) => {
    trx
      .insert({
        hash: hash,
        email: email,
      })
      .into('login')
      .returning('email')
      .then((loginEmail) => {
        return trx('users')
          .returning('*')
          .insert({
            email: loginEmail[0],
            name: name,
            joined: new Date(),
          })
          .then((user) => {
            res.json(user[0]);
          });
      })
      .then(trx.commit)
      .catch(trx.rollback);
  }).catch((err) => {
    if (err.constraint === 'login_email_key') {
      res.status(400).json('Email already registered');
    } else {
      res.status(400).json('Unable to register');
    }
  });
});

app.get('/getCharts/:id', (req, res) => {
  const { id } = req.params;
  db.select('*')
    .from('charts')
    .where('charts.userid', '=', id)
    .orderBy('chart_id', 'asc')
    .then((charts) => res.json(charts))
    .catch((err) => res.status(400).json('unable to get user'));
});

app.post('/addChart', (req, res) => {
  const {
    userid,
    title,
    type,
    data1,
    data2,
    data3,
    data4,
    data5,
    data6,
    data7,
    data8,
    data9,
    data10,
  } = req.body;
  db.insert({
    userid: userid,
    title: title,
    type: type,
    data1: data1,
    data2: data2,
    data3: data3,
    data4: data4,
    data5: data5,
    data6: data6,
    data7: data7,
    data8: data8,
    data9: data9,
    data10: data10,
  })
    .into('charts')
    .returning('*')
    .then((info) => res.status(200).json(info[0]))
    .catch((err) => {
      console.log(err);
      res.status(400).json('unable to create a new Chart, please try again');
    });
});

app.put('/updateChart/:id', (req, res) => {
  const {
    title,
    type,
    data1,
    data2,
    data3,
    data4,
    data5,
    data6,
    data7,
    data8,
    data9,
    data10,
  } = req.body;
  const { id } = req.params;
  db('charts')
    .where('charts.chart_id', '=', id)
    .update({
      title: title,
      type: type,
      data1: data1,
      data2: data2,
      data3: data3,
      data4: data4,
      data5: data5,
      data6: data6,
      data7: data7,
      data8: data8,
      data9: data9,
      data10: data10,
    })
    .returning('*')
    .then((chart) => res.status(200).json(chart[0]))
    .catch((err) => res.status(400).json('unable to get Chart'));
});

app.delete('/deleteChart', (req, res) => {
  const { id } = req.body;
  db('charts')
    .where('charts.chart_id', '=', id)
    .del()
    .then((charts) => res.json(charts))
    .catch((err) => console.log(err));
});

app.listen(port, () => console.log('Server started at port:' + port));
